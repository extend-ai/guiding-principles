# Guiding Principles

To make our vision a reality we are dedicated to create an environment where everyone involved finds meaning and fulfilment. Culture is vitally important, our guiding principles are part of our bi-weekly sprints, ensuring we continuously improve and adapt ourselves as well as the product. We also decided to open source it to share with others our discoveries of the human mind.

In the coming weeks, we will codify our guiding principles in a Jupyter notebook and we will expand on these core principles: 

- Radical candor
- Be vulnerable
- To go fast, take your time
- Intuition is the user, computation is the power


